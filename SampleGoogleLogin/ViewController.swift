//
//  ViewController.swift
//  SampleGoogleLogin
//
//  Created by Etsushi Otani on 2021/08/17.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController {

    @IBOutlet weak var signInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    @IBAction func signIn(_ sender: GIDSignInButton) {
        LoginManager().signIn(present: self)
    }
    
}

