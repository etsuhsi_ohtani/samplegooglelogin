//
//  LoginManager.swift
//  SampleGoogleLogin
//
//  Created by Etsushi Otani on 2021/08/17.
//

import GoogleSignIn

class LoginManager {
    
    static let signInConfig = GIDConfiguration.init(clientID: "713524222077-98btj31ahb3anv8amji98h23fj68brno.apps.googleusercontent.com")
    
    func signIn(present: UIViewController) {
        GIDSignIn.sharedInstance.signIn(with: LoginManager.signInConfig, presenting: present, callback: { user, error in
            guard error == nil else {
                // エラー処理
                print("##### Login error #####")
                return
            }
            //成功時の処理：アクセストークンを取得できる
            print("##### Login success #####")
            print(user?.authentication.accessToken)
        })
    }
    
    func signOut() {
        GIDSignIn.sharedInstance.signOut()
    }
}
